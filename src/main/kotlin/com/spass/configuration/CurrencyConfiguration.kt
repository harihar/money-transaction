package com.spass.configuration

val supportedCurrencies = listOf(
    "EUR",
    "USD",
    "INR",
    "GBP",
    "RUB",
    "JPY",
    "CNY",
    "BRL",
    "CHF",
    "SEK",
    "THB",
    "KWD",
    "CZK"
)
