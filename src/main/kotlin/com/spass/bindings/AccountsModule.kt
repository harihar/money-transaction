package com.spass.bindings

import com.google.inject.*
import com.spass.controller.AccountsController
import com.spass.services.AccountService
import javax.sql.DataSource

class AccountsModule : PrivateModule() {

    override fun configure() {
    }

    @Provides
    @Singleton
    @Exposed
    fun accountService(dataSource: DataSource): AccountService {
        return AccountService(dataSource)
    }

}
