package com.spass

import com.google.inject.Guice
import com.spass.bindings.AccountsModule
import com.spass.bindings.CommonModule
import com.spass.bindings.TransactionModule
import com.spass.controller.AccountsController
import com.spass.controller.TransactionController
import org.flywaydb.core.Flyway
import org.jooq.impl.DSL
import org.slf4j.LoggerFactory
import javax.sql.DataSource

val logger = LoggerFactory.getLogger("com.spass.Main")

fun main() {
    logger.info("Starting main")
    val injector = Guice.createInjector(
        CommonModule(),
        AccountsModule(),
        TransactionModule()
    )
    injector.injectMembers(AccountsController())
    injector.injectMembers(TransactionController())

    injector.getInstance(Flyway::class.java).migrate()

    insertSeedDataInDB(injector.getInstance(DataSource::class.java))
}

private fun insertSeedDataInDB(dataSource: DataSource) {
    logger.info("==Starting seed data load==")
    val accountsSeedDataSql = object {}.javaClass
        .classLoader
        .getResource("db.seed/accounts.sql")?.readText()
    DSL.using(dataSource.connection)
        .execute(accountsSeedDataSql)
    logger.info("==Finished seed data loading==")
}
