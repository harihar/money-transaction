package com.spass.model

import org.javamoney.moneta.Money

data class Account(val accountId: String, val balance: Money)