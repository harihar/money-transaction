package com.spass.model

data class ErrorResponse(val errorCode: String, val errorMessage: String)