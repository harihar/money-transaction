package com.spass.model

data class AccountsResponse(val accounts: List<Account>)