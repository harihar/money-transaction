package com.spass.model

data class TransactionResponse(val transactionId: String)
