package com.spass.controller

import com.spass.model.AccountsResponse
import com.spass.services.AccountService
import com.spass.utils.GsonUtil
import spark.Spark.get
import javax.inject.Inject

class AccountsController {

    @Inject
    lateinit var accountService: AccountService

    init {
        get("/accounts") { _, res ->
            res.type("application/json")
            GsonUtil.gson.toJson(AccountsResponse(accountService.getAccounts()))
        }

        get("/accounts/:accountId") { req, res ->
            val accountId = req.params("accountId")
            val account = accountService.getAccount(accountId)
            if (account == null) {
                res.status(404)
            } else {
                res.type("application/json")
                GsonUtil.gson.toJson(account)
            }
        }
    }
}

