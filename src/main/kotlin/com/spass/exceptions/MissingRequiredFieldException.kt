package com.spass.exceptions

import java.lang.RuntimeException

class MissingRequiredFieldException(val fieldName: String) : RuntimeException()
