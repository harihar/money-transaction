package com.spass.exceptions

import java.lang.RuntimeException

class UnsupportedCurrencyException : RuntimeException()
