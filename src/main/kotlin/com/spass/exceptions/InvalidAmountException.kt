package com.spass.exceptions

import java.lang.RuntimeException

class InvalidAmountException : RuntimeException()