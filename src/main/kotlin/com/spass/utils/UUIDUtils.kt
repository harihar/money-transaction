package com.spass.utils

import java.util.*

fun randomUUIDString() = UUID.randomUUID().toString()
